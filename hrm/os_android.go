package main

import (
	"log"

	"gioui.org/app"
	"gioui.org/io/event"

	"git.wow.st/gmp/ble"
)

var confDir string
var fontSize float32 = 24

func getConfDir() string {
	if confDir == "" {
		var err error
		confDir,err = app.DataDir()
		if err != nil {
			log.Fatal("Cannot get configuration directory")
		}
	}
	return confDir
}

func handleEvent(e event.Event, b *ble.BLE) {
	switch e:= e.(type) {
	case app.ViewEvent:
		b.Enable(e.View)
	}
}
