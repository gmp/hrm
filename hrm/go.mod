module st.wow.git/gmp/hrm/hrm

go 1.16

require (
	gioui.org v0.0.0-20211113093644-40bc2e1f88b8
	gioui.org/cmd v0.0.0-20211113093644-40bc2e1f88b8
	git.wow.st/gmp/ble v0.0.0-20211115204235-6da0474e59d6
	gopkg.in/yaml.v2 v2.3.0
)
