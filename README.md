# HRM

An example heart rate monitor app for Android and MacOS using [git.wow.st/gmp/ble](https://git.wow.st/gmp/ble) and [Gio](https://gioui.org).

The MacOS implementation uses a Core Bluetooth binding created by
[NSWrap](https://git.wow.st/gmp/nswrap). On Android, Martijn van Welie's
[Blessed](https://github.com/weliem/blessed-android) library is used to
simplify access to the Bluetooth Low-Energy hardware.

## MacOS:

```
git clone https://git.wow.st/gmp/hrm
cd hrm/hrm
go build
./hrm
```

## Android:

The `gogio` command from `gioui.org/cmd/gogio` must be installed to build
an Android APK.

```
git clone https://git.wow.st/gmp/hrm
cd hrm/hrm
gogio -target android .
adb install -r hrm.apk
```

The compiled Java code for the Android implementation is pre-packaged in
jar files. If you want to, you can re-compile these dependencies by running
`go generate` from the `depjars` directory. You will need to have `gradle`,
an Android SDK, and who knows what else installed on your machine.
